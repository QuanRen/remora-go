package dao

import (
	"time"
)

type RuleError interface {
	Code() string
	error
}

type person struct {
	Key, Name string
}

type DateRange struct {
	Start    time.Time
	Interval time.Duration
}

func (dr DateRange) Duration(ddr DateRange) time.Duration {
	return dr.Start.Sub(ddr.Start)
}

type ShiftType string

const (
	OffDay          = ShiftType("offDay")
	OfficialHoliday = ShiftType("official")
	WorkDay         = ShiftType("work")
)

type Shift struct {
	Title     string
	Address   string
	DateRange `yaml:"dateRange"`
	Typ       ShiftType `yaml:"type"`

	isOverlap bool
	Owner     []*person
	RuleErr   RuleError
}

func (s *Shift) SetErr(re RuleError) {
	s.RuleErr = re
}

func (s *Shift) HasErr() bool {
	return s.RuleErr != nil
}

func (s *Shift) InRange(start, end time.Time) bool {
	return s.Start.Sub(start) >= 0 && s.Start.Sub(end) <= 0
}

func (s *Shift) IsOverlap(ss Shift) bool {
	d := s.Duration(ss.DateRange)
	if d < 0 {
		d = d * -1
	}
	return d < s.Interval
}

type ShiftSchedule []*Shift

func (ss *ShiftSchedule) Add(sary ...*Shift) {
	if ss == nil {
		ss = &ShiftSchedule{}
	}
	for _, s := range sary {
		*ss = append(*ss, s)
	}
}

func (ss ShiftSchedule) TotalMinute() uint16 {
	var td time.Duration
	for _, s := range ss {
		td = td + s.Interval
	}
	return uint16(td / time.Minute)
}

func (ss ShiftSchedule) GetOverlap() []*Shift {
	var result []*Shift
	l := len(ss)
	var i, j int
	for i = 0; i < l; i++ {
		ss[i].isOverlap = false
	}
	for i = 0; i < l-1; i++ {
		for j = i + 1; j < l; j++ {
			if ss[i].IsOverlap(*ss[j]) {
				if !ss[i].isOverlap {
					result = append(result, ss[i])
					ss[i].isOverlap = true
				}
				if !ss[j].isOverlap {
					result = append(result, ss[j])
					ss[j].isOverlap = true
				}
			}
		}
	}
	return result
}
