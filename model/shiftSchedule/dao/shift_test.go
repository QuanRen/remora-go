package dao

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_ShiftOverlap(t *testing.T) {
	s1 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 0, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s2 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 30, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s3 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 14, 0, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	assert.True(t, s1.IsOverlap(s2))
	assert.False(t, s1.IsOverlap(s3))
}

func Test_ShiftScheduleTotalHour(t *testing.T) {
	s1 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 0, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s2 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 30, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s3 := Shift{
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 14, 0, 0, 0, time.Now().Location()),
			Interval: time.Minute * 30,
		},
	}
	var ss ShiftSchedule
	ss.Add(&s1, &s2, &s3)
	assert.Equal(t, uint16(150), ss.TotalMinute())
}

func Test_ShiftScheduleOverlap(t *testing.T) {
	s1 := Shift{
		Title: "s1",
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 0, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s2 := Shift{
		Title: "s2",
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 13, 30, 0, 0, time.Now().Location()),
			Interval: time.Hour,
		},
	}
	s3 := Shift{
		Title: "s3",
		DateRange: DateRange{
			Start:    time.Date(2022, 5, 20, 14, 0, 0, 0, time.Now().Location()),
			Interval: time.Minute * 30,
		},
	}
	var ss ShiftSchedule
	ss.Add(&s1, &s2, &s3)
	overlap := ss.GetOverlap()
	assert.True(t, overlap[0].Start.Equal(s1.Start))
	assert.Equal(t, "s1", overlap[0].Title)
	assert.Equal(t, 3, len(overlap))
}
