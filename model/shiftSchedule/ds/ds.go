package ds

import (
	"remora-go/model/shiftSchedule/dao"
	"time"
)

type ShiftScheduleDS interface {
	GetScheduleByOwner(ownerKey string, start, end time.Time) dao.ShiftSchedule
}

func NewMockDS(s []*dao.Shift) ShiftScheduleDS {
	return &mockDS{s}
}

type mockDS struct {
	dao.ShiftSchedule
}

func (ds *mockDS) GetScheduleByOwner(ownerKey string, start, end time.Time) dao.ShiftSchedule {
	return ds.ShiftSchedule
}
