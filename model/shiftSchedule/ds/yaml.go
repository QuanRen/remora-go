package ds

import (
	"errors"
	"io/ioutil"
	"remora-go/model/shiftSchedule/dao"
	"remora-go/util"
	"time"

	"gopkg.in/yaml.v2"
)

func NewYamlDS(file string) (ShiftScheduleDS, error) {
	if !util.FileExists(file) {
		return nil, errors.New("file not exist: " + file)
	}
	yamlFile, err := ioutil.ReadFile(file)
	if err != nil {
		return nil, err
	}
	ss := dao.ShiftSchedule{}
	err = yaml.Unmarshal(yamlFile, &ss)
	if err != nil {
		return nil, err
	}
	return &yamlDS{
		ShiftSchedule: ss,
	}, nil
}

type yamlDS struct {
	dao.ShiftSchedule
}

func (ds *yamlDS) GetScheduleByOwner(ownerKey string, start, end time.Time) dao.ShiftSchedule {
	result := dao.ShiftSchedule{}
	for _, s := range ds.ShiftSchedule {
		for _, p := range s.Owner {
			if p.Key == ownerKey && s.InRange(start, end) {
				result.Add(s)
			}
		}
	}
	return result
}
