package ds

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func Test_YamlDS(t *testing.T) {

	ds, err := NewYamlDS("../test/test1.yml")
	assert.Nil(t, err)

	start, _ := time.Parse(time.RFC3339, "2021-05-01T08:00:00+08:00")
	end, _ := time.Parse(time.RFC3339, "2021-05-02T08:00:00+08:00")

	r := ds.GetScheduleByOwner("peter", start, end)
	assert.Equal(t, 2, len(r))
	r = ds.GetScheduleByOwner("john", start, end)
	assert.Equal(t, 1, len(r))
}
