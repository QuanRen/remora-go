package salary

import (
	"remora-go/model/salary/dao"
	scheDao "remora-go/model/shiftSchedule/dao"
)

type SalaryType string

const (
	TypeDaily = SalaryType("daily")
	TypeHour  = SalaryType("hour")
	TypeMonth = SalaryType("month")
)

type SalaryConf interface {
	Type() SalaryType
	GetHourPay() float64
}

type Salary interface {
	// 設定班表
	SetShiftSchedule(ss scheDao.ShiftSchedule)
	// 取得加班清單
	GetOvertimeSchedule() []*dao.SalaryShift
	// 加班費
	OvertimePay() float64
	// 基本薪資
	BasicPay() float64
}

func New(c SalaryConf, ss scheDao.ShiftSchedule) Salary {
	var salary Salary
	switch c.Type() {
	case TypeHour:
		salary = newHourSalary(c.GetHourPay())
	default:
		return nil
	}
	salary.SetShiftSchedule(ss)
	return salary
}

func TrasSalaryShiftSchedule(ss scheDao.ShiftSchedule) dao.SalaryShiftSchedule {
	return nil
}

type commonSalary struct {
	salarySchedule dao.SalaryShiftSchedule
	hourPay        float64
}

// 加班費
func (hs *commonSalary) OvertimePay() float64 {
	var pay float64
	for _, ss := range hs.salarySchedule {
		if ss.Multiple == 1 {
			continue
		}
		pay += ss.Multiple * hs.hourPay * ss.Interval.Hours()
	}
	return pay
}

// 基本薪資
func (hs *commonSalary) BasicPay() float64 {
	var workHour float64
	for _, ss := range hs.salarySchedule {
		if ss.Multiple > 1 {
			continue
		}
		workHour += ss.Interval.Hours()
	}
	return workHour * hs.hourPay
}
