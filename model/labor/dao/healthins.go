package dao

import (
	"math"
)

// 勞保被保險人類別
type HealthInsType string

const (
	// 公務人員、公職人員
	HealthInsTypePublicOfficials = HealthInsType("publicOfficials")
	// 私校教職員
	HealthInsTypePrivateSchool = HealthInsType("privateSchool")
	// 公民營事業、機構等有一定雇主的受雇者
	HealthInsTypeEmployee = HealthInsType("employee")
	// 雇主、自營業主、專門職業及技術人員自行執業者
	HealthInsTypeHirer = HealthInsType("hirer")
	// 職業工會會員、外僱船員
	HealthInsTypeSecond = HealthInsType("second")
	// 農、漁會會員
	HealthInsTypeThird = HealthInsType("third")
	// 義務役軍人、替代役役男、軍校軍費生、在卹遺眷、在矯正機關接受刑或保安處分(保護管束除外)、管訓處分之執行逾2個月者
	// 已領老年給付自願加職災保險人員
	HealthInsTypeFourth = HealthInsType("fourth")
	// 低收入戶
	HealthInsTypeFifth = HealthInsType("fifth")
	// 榮民、榮民遺眷家戶代表
	HealthInsTypeVeterans = HealthInsType("veterans")
	// 其他地區人口
	HealthInsTypeOther = HealthInsType("other")
)

// 補助項目
type SubsidyObject string

const (
	// 無補助
	SubsidyNone = SubsidyObject("none")
	// 70歲以上中低收入老人
	SubsidyLowIncome70Old = SubsidyObject("lowIncome70Old")
	// 輕度身心障礙
	SubsidyDisablilityMild = SubsidyObject("disablilityMild")
	// 中度
	SubsidyDisabilityModerate = SubsidyObject("disablilityModerate")
	// (極)重度
	SubsidyDisabilitySevere = SubsidyObject("disablilitySevere")
	// 中低收入戶
	SubsidyLowIncome = SubsidyObject("lowIncome")
	// 中低收入戶未滿18歲兒童及少年
	SubsidyLowIncomeUnder18 = SubsidyObject("lowIncomeUnder18")
)

type HealthIns struct {
	Year int
	Desc string
	// 費率
	HealthRate float64 `json:"healthRate"`
	// 平均眷口數
	AverageOfHouseholds float64 `json:"averageOfHouseholds"`
	// 平均保險費
	AverageInsurance int `json:"averageInsurance"`
	// 分擔比例
	ShareMap map[HealthInsType]*healthShareDetail
	// 投保級距
	Grade []int
	// 補助比例
	SubsidyRate map[SubsidyObject]float64
}

type healthShareDetail struct {
	// 個人負擔比例
	Self []float64
	// 眷屬負擔比例
	Households []float64
}

type CalInput struct {
	// 薪資
	Salary int
	// 被保險人類別
	Typ HealthInsType
	// 被保險人補助項目
	Subsidy SubsidyObject
	// 所有眷屬補助項目
	HouseholdsSubsidy []SubsidyObject
}

type insuredDetail struct {
	// 保險金
	Insurance int
	// 補助扣除
	SubsidyAmount int
}

type HealthInsCalResult struct {
	// 投保金額
	Insured int
	Labor   insuredDetail
	Organ   int
}

func (l *HealthIns) Calculate(ci *CalInput) *HealthInsCalResult {
	share := l.ShareMap[ci.Typ]
	result := &HealthInsCalResult{}
	switch ci.Typ {
	case HealthInsTypeVeterans, HealthInsTypeOther:
		result.Insured = l.AverageInsurance
		insurance, subsidy := l.getAverageLabor(ci.Subsidy, ci.HouseholdsSubsidy, share)
		result.Labor = insuredDetail{
			Insurance:     insurance,
			SubsidyAmount: subsidy,
		}
	default:
		insured := l.getGrade(ci.Salary)
		insurance, subsidy := l.getLabor(insured, ci.Subsidy, ci.HouseholdsSubsidy, share)
		result.Insured = insured
		result.Organ = l.getOrgan(insured, share)
		result.Labor = insuredDetail{
			Insurance:     insurance,
			SubsidyAmount: subsidy,
		}
	}

	return result
}

func (l *HealthIns) getAverageLabor(selfSubsidy SubsidyObject, households []SubsidyObject, share *healthShareDetail) (insurance int, subsidy int) {
	floatInsured := float64(l.AverageInsurance)
	householderNum := len(households)
	// 超過3個眷屬以三個眷屬算
	if householderNum > 3 {
		householderNum = 3
	}
	insurance = int(math.Round(floatInsured*share.Self[0])) +
		int(math.Round(floatInsured*share.Households[0]))*householderNum
	return
}

func (l *HealthIns) getLabor(insured int, selfSubsidy SubsidyObject, households []SubsidyObject, share *healthShareDetail) (insurance int, subsidy int) {
	floatInsured := float64(insured)
	householderNum := len(households)
	// 超過3個眷屬以三個眷屬算
	if householderNum > 3 {
		householderNum = 3
	}
	selfInsurance := int(math.Round(floatInsured * l.HealthRate / 100 * share.Self[0]))
	holdsInsurance := int(math.Round(floatInsured * l.HealthRate / 100 * share.Households[0]))
	insurance = selfInsurance + holdsInsurance*householderNum

	subsidyRate, ok := l.SubsidyRate[selfSubsidy]
	if ok {
		subsidy += int(math.Round(subsidyRate * float64(selfInsurance)))
	}

	// 選出條件最大的三個補助金額
	subsidyList := [3]int{}
	ll := len(subsidyList)
	var currSubsidy, temp int
	for _, so := range households {
		subsidyRate, ok := l.SubsidyRate[so]
		if !ok {
			continue
		}
		currSubsidy = int(math.Round(subsidyRate * float64(holdsInsurance)))
		for i := 0; i < ll; i++ {
			if subsidyList[i] == 0 {
				subsidyList[i] = currSubsidy
				break
			}
			if subsidyList[i] < currSubsidy {
				temp = subsidyList[i]
				subsidyList[i] = currSubsidy
				currSubsidy = temp
				continue
			}
		}
	}
	for _, s := range subsidyList {
		subsidy += s
	}
	return
}

func (l *HealthIns) getOrgan(insured int, share *healthShareDetail) int {
	return int(math.Round(float64(insured) * l.HealthRate * share.Self[1] / 100.0 * (1 + l.AverageOfHouseholds)))
}

// 取得投保階距
func (l *HealthIns) getGrade(salary int) int {
	gl := len(l.Grade)
	for i := 0; i < gl; i++ {
		if salary <= l.Grade[i] {
			return l.Grade[i]
		}
	}
	return l.Grade[gl-1]
}
