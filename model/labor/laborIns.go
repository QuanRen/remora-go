package labor

import (
	"net/http"
	"remora-go/core/api"
	coreDao "remora-go/core/dao"
	"remora-go/core/log"
	"remora-go/dao/doc"
	"remora-go/model/core"
	"remora-go/model/labor/dao"
	"remora-go/util"
)

type LaborIns interface {
	Save(typ string, name string, ins dao.LaborIns, u coreDao.LogUser) error
	Load(year int, typ string) error
	Calculate(salary int, laborTyp dao.LaborInsType, dl dao.DisabilityLevel, injuryRate float64, days uint8) *dao.LaborInsCalResult
}

func NewLaborInsMongo(mgo core.MgoDBModel, log log.Logger) LaborIns {
	return &laborInsMongo{
		mgo: mgo,
		log: log,
	}
}

type laborInsMongo struct {
	mgo core.MgoDBModel
	log log.Logger

	*dao.LaborIns
}

func (li *laborInsMongo) Load(year int, typ string) error {
	id, err := doc.GetLaborInsId(year, typ)
	if err != nil {
		return api.NewApiError(http.StatusBadRequest, err.Error())
	}
	d := &doc.LaborIns{
		ID: id,
	}
	err = li.mgo.FindByID(d)
	if err != nil {
		return api.NewApiError(http.StatusNotFound, err.Error())
	}
	li.LaborIns = &d.LaborIns
	return nil
}

func (li *laborInsMongo) Save(typ string, name string, ins dao.LaborIns, u coreDao.LogUser) error {
	if !util.IsStrInList(typ, doc.AllowLaborInsType...) {
		return api.NewApiError(http.StatusBadRequest, "invalid type: "+typ)
	}
	d := &doc.LaborIns{}
	var err error
	d.ID, err = doc.GetLaborInsId(ins.Year, typ)
	if err := li.mgo.FindByID(d); err == nil {
		li.mgo.RemoveByID(d, u)
	}
	d = &doc.LaborIns{
		Typ:      typ,
		Name:     name,
		LaborIns: ins,
	}
	_, err = li.mgo.Save(d, u)
	if err != nil {
		return api.NewApiError(http.StatusInternalServerError, err.Error())
	}
	li.LaborIns = &d.LaborIns
	return nil
}
