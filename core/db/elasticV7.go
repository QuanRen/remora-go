package db

import (
	"net/http"
	"remora-go/util"

	elastic "github.com/olivere/elastic/v7"
)

const (
	CtxElasticKey = util.CtxKey("ctxElasticKey")
)

func GetCtxElastic(req *http.Request) *elastic.Client {
	ctx := req.Context()
	cltInter := ctx.Value(CtxElasticKey)

	if clt, ok := cltInter.(*elastic.Client); ok {
		return clt
	}
	return nil
}

type Searchv7 struct {
	Url  string `yaml:"url"`
	User string
	Pwd  string

	client *elastic.Client
}

func (s *Searchv7) GetClient() *elastic.Client {
	if s.client != nil {
		return s.client
	}

	c := s.connect()
	s.client = c

	return c
}

func (s *Searchv7) connect() *elastic.Client {
	myclient, err := elastic.NewClient(
		elastic.SetURL(s.Url),
		elastic.SetSniff(false),
		elastic.SetBasicAuth(s.User, s.Pwd))
	if err != nil {
		panic(err)
	}
	return myclient
}
