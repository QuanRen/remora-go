package core

import (
	"fmt"
	"io/ioutil"
	"os"
	"remora-go/core/db"
	"time"

	"remora-go/core/api"
	"remora-go/core/auth"
	"remora-go/core/log"
	"remora-go/util"

	"github.com/olivere/elastic/v7"
	yaml "gopkg.in/yaml.v2"
)

type DI interface {
	db.MongoDI
	log.LoggerDI
	api.ApiDI
	auth.TransmitSecurity
}

type di struct {
	db.MongoConf               `yaml:"mongo,omitempty"`
	Search                     *db.Searchv7 `yaml:"search,omitempty"`
	*log.LoggerConf            `yaml:"log,omitempty"`
	*api.APIConf               `yaml:"api,omitempty"`
	*auth.TransmitSecurityConf `yaml:"transmitSecurity"`
	JWTConf                    *api.JwtConf `yaml:"jwtConf"`
}

func (d *di) GetSearch() *elastic.Client {
	if d == nil || d.Search == nil {
		panic("not set search")
	}
	return d.Search.GetClient()
}

func (d *di) GetJWTConf() *api.JwtConf {
	if d.JWTConf == nil {
		panic("apiConf not set")
	}
	return d.JWTConf
}

var mydi *di

func GetDI() DI {
	if mydi == nil {
		panic("not init di")
	}
	return mydi
}

func InitConfByFile(f string) {
	yamlFile, err := ioutil.ReadFile(f)
	if err != nil {
		panic(err)
	}
	mydi = &di{}
	err = yaml.Unmarshal(yamlFile, mydi)
	if err != nil {
		panic(err)
	}
	util.InitValidator()
}

var _env = ""

// 初始化設定檔，讀YAML檔
func IniConfByEnv(path, env string) {
	const confFileTpl = "%s/%s/config.yml"
	_env = env
	InitConfByFile(fmt.Sprintf(confFileTpl, path, env))
}

func InitDefaultConf(path string) {
	InitConfByFile(util.StrAppend(path, "/conf/config.yml"))
}

func IsTest() bool {
	return _env == "test"
}

func SystemPwd() string {
	md5pwd := os.Getenv("ENCR_WORD")
	return md5pwd
}

func (d *di) Close(key string) {

}

type modbusConf struct {
	ReadDuration  string `yaml:"rd"`
	WriteDuration string `yaml:"wd"`
}

func (mc modbusConf) GetReadDuration() time.Duration {
	d, err := time.ParseDuration(mc.ReadDuration)
	if err != nil {
		panic(err)
	}
	return d
}

func (mc modbusConf) GetWriteDuration() time.Duration {
	d, err := time.ParseDuration(mc.WriteDuration)
	if err != nil {
		panic(err)
	}
	return d
}
