module remora-go

go 1.15

require (
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/google/uuid v1.1.2
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/olivere/elastic/v7 v7.0.24
	github.com/pquerna/otp v1.3.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.5.3
	gopkg.in/yaml.v2 v2.2.8
)
