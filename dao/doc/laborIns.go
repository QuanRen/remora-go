package doc

import (
	"encoding/binary"
	"errors"

	"remora-go/core/dao"
	laborDao "remora-go/model/labor/dao"
	"remora-go/util"

	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

const (
	laborInsC = "laborIns"

	// 一般勞工
	LaborInsTypeNormal = "normal"
	// 部份工時勞工
	LaborInsTypePartial = "partial"
)

var (
	AllowLaborInsType = []string{
		LaborInsTypeNormal, LaborInsTypePartial,
	}
)

type LaborIns struct {
	ID   primitive.ObjectID `bson:"_id"`
	Typ  string
	Name string
	laborDao.LaborIns

	dao.CommonDoc `bson:"meta"`
}

func GetLaborInsId(year int, typ string) (primitive.ObjectID, error) {
	if !util.IsStrInList(typ, AllowLaborInsType...) {
		return primitive.NilObjectID, errors.New("invalid typ: " + typ)
	}
	var b [12]byte
	bs := make([]byte, 2)
	binary.BigEndian.PutUint16(bs, uint16(year))
	for i := 0; i < 2; i++ {
		b[i] = bs[i]
	}
	typLen := len(typ)
	const fixLen = 6
	if typLen < fixLen {
		l := fixLen - typLen
		for i := 0; i < l; i++ {
			typ = util.StrAppend(typ, "_")
		}
	}
	tb := []byte(typ[:6])
	for i := 0; i < fixLen; i++ {
		b[i+2] = tb[i]
	}
	return b, nil
}

func (d *LaborIns) newID() primitive.ObjectID {
	id, err := GetLaborInsId(d.Year, d.Typ)
	if err != nil {
		panic(err)
	}
	return id
}

func (u *LaborIns) GetDoc() interface{} {
	if u.ID.IsZero() {
		u.ID = u.newID()
	}
	return u
}

func (u *LaborIns) GetC() string {
	return laborInsC
}

func (u *LaborIns) GetID() primitive.ObjectID {
	return u.ID
}

func (u *LaborIns) GetIndexes() []mongo.IndexModel {
	return []mongo.IndexModel{}
}
