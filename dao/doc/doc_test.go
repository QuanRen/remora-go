package doc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_GetLaborInsId(t *testing.T) {

	id, err := GetLaborInsId(110, "aab")
	assert.NotNil(t, err)

	id, err = GetLaborInsId(110, LaborInsTypeNormal)
	assert.Nil(t, err)
	assert.Equal(t, "006e6e6f726d616c00000000", id.Hex())
}
