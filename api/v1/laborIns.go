package v1

import (
	"encoding/json"
	"net/http"
	"remora-go/api/input"
	"remora-go/core/api"
	"remora-go/core/db"
	"remora-go/core/log"
	"remora-go/dao/doc"
	"remora-go/model/core"
	"remora-go/model/labor"
)

func NewLaborInsAPI(l log.Logger) api.API {
	return &laborInsAPI{
		l: l,
	}
}

type laborInsAPI struct {
	l log.Logger
}

func (app *laborInsAPI) GetName() string {
	return "laborIns"
}

func (app *laborInsAPI) GetAPIs() []*api.APIHandler {
	return []*api.APIHandler{
		{Path: "/v1/laborIns/normal", Next: app.createNormalInsEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/laborIns/normal", Next: app.getNormalInsEndpoint, Method: "GET", Auth: true},
		{Path: "/v1/laborIns/partial", Next: app.createPartialInsEndpoint, Method: "POST", Auth: true},
		{Path: "/v1/laborIns/partial", Next: app.getPartialInsEndpoint, Method: "GET", Auth: true},
	}
}

func (app *laborInsAPI) createNormalInsEndpoint(w http.ResponseWriter, req *http.Request) {
	inp := &input.CreateLaborIns{}
	err := json.NewDecoder(req.Body).Decode(&inp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = inp.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	l := log.GetCtxLog(req)
	dbm := core.NewMgoModelByReq(req, db.CoreDB)
	li := labor.NewLaborInsMongo(dbm, l)
	ui := input.GetUserInfo(req)
	err = li.Save(doc.LaborInsTypeNormal, inp.Name, inp.LaborIns, ui)
	if err != nil {
		api.OutputErr(w, err)
		return
	}
	w.Write([]byte("ok"))
}

func (app *laborInsAPI) getNormalInsEndpoint(w http.ResponseWriter, req *http.Request) {
	// qv := util.GetQueryValue(req, []string{"salary", "type", "days", "year"}, true)
	// query, err := input.ParserQueryLaborIns(
	// 	qv["year"].(string),
	// 	qv["salary"].(string),
	// 	qv["type"].(string),
	// 	qv["days"].(string),
	// )
	// if err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
	// if err = query.Validate(); err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
	// l := log.GetCtxLog(req)
	// dbm := core.NewMgoModelByReq(req, db.CoreDB)
	// li := labor.NewLaborInsMongo(dbm, l)
	// err = li.Load(query.Year, doc.LaborInsTypeNormal)
	// if err != nil {
	// 	api.OutputErr(w, err)
	// 	return
	// }
	// insured := li.GetGrade(query.Salary)
	// var labor, organ int
	// fmt.Println(query.Days, query.InsTyp)
	// switch query.InsTyp {
	// case input.InsTypAccident:
	// 	labor, organ = li.GetAccident(insured, query.Days)
	// case input.InsTypEmployemnt:
	// 	labor, organ = li.GetEmployee(insured, query.Days)
	// default:
	// 	labor, organ = li.GetAccidentAndEmployee(insured, query.Days)
	// }
	// w.Header().Set("Content-Type", "application/json")
	// err = json.NewEncoder(w).Encode(map[string]interface{}{
	// 	"insured": insured,
	// 	"labor":   labor,
	// 	"organ":   organ,
	// })
	// if err != nil {
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
}

func (app *laborInsAPI) createPartialInsEndpoint(w http.ResponseWriter, req *http.Request) {
	inp := &input.CreateLaborIns{}
	err := json.NewDecoder(req.Body).Decode(&inp)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	if err = inp.Validate(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}
	l := log.GetCtxLog(req)
	dbm := core.NewMgoModelByReq(req, db.CoreDB)
	li := labor.NewLaborInsMongo(dbm, l)
	ui := input.GetUserInfo(req)
	err = li.Save(doc.LaborInsTypePartial, inp.Name, inp.LaborIns, ui)
	if err != nil {
		api.OutputErr(w, err)
		return
	}
	w.Write([]byte("ok"))
}

func (app *laborInsAPI) getPartialInsEndpoint(w http.ResponseWriter, req *http.Request) {
	// qv := util.GetQueryValue(req, []string{"salary", "type", "days", "year"}, true)
	// query, err := input.ParserQueryLaborIns(
	// 	qv["year"].(string),
	// 	qv["salary"].(string),
	// 	qv["type"].(string),
	// 	qv["days"].(string),
	// )
	// if err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
	// if err = query.Validate(); err != nil {
	// 	w.WriteHeader(http.StatusBadRequest)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
	// l := log.GetCtxLog(req)
	// dbm := core.NewMgoModelByReq(req, db.CoreDB)
	// li := labor.NewLaborInsMongo(dbm, l)
	// err = li.Load(query.Year, doc.LaborInsTypePartial)
	// if err != nil {
	// 	api.OutputErr(w, err)
	// 	return
	// }
	// insured := li.Calculate(query.Salary, query.InsTyp, )
	// var labor, organ int
	// switch query.InsTyp {
	// case input.InsTypAccident:
	// 	labor, organ = li.GetAccident(insured, query.Days)
	// case input.InsTypEmployemnt:
	// 	labor, organ = li.GetEmployee(insured, query.Days)
	// default:
	// 	labor, organ = li.GetAccidentAndEmployee(insured, query.Days)
	// }
	// w.Header().Set("Content-Type", "application/json")
	// err = json.NewEncoder(w).Encode(map[string]interface{}{
	// 	"insured": insured,
	// 	"labor":   labor,
	// 	"organ":   organ,
	// })
	// if err != nil {
	// 	w.WriteHeader(http.StatusInternalServerError)
	// 	w.Write([]byte(err.Error()))
	// 	return
	// }
}
