package input

import (
	"errors"
	"net/http"
	"remora-go/core/dao"
	"remora-go/util"
)

const (
	CtxUserInfoKey = util.CtxKey("userInfo")
)

type ReqUser interface {
	dao.LogUser
	GetId() string
	GetPerm() string
	GetVat() string
	GetStore() string
}

type reqUserImpl struct {
	Vat     string
	Store   string
	Id      string
	Account string
	Name    string
	Perm    string
}

func (ru reqUserImpl) GetId() string {
	return ru.Id
}

func (ru reqUserImpl) GetName() string {
	return ru.Name
}

func (ru reqUserImpl) GetAccount() string {
	return ru.Account
}

func (ru reqUserImpl) GetPerm() string {
	return ru.Perm
}

func (ru reqUserImpl) GetVat() string {
	return ru.Vat
}

func (ru reqUserImpl) GetStore() string {
	return ru.Store
}

func NewReqUser(vat, storeid, uid, acc, name, perm string) ReqUser {
	return reqUserImpl{
		Account: acc,
		Store:   storeid,
		Id:      uid,
		Name:    name,
		Perm:    perm,
		Vat:     vat,
	}
}

func GetUserInfo(req *http.Request) ReqUser {
	ctx := req.Context()
	reqID := ctx.Value(CtxUserInfoKey)
	if ret, ok := reqID.(ReqUser); ok {
		return ret
	}
	return nil
}

type BasicLogin struct {
	Account string
	Pwd     string
}

func (qb *BasicLogin) Validate() error {
	if qb.Account == "" {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss password")
	}
	return nil
}

type Register struct {
	Account string
	Pwd     string
	Perm    string
	Key     string
}

func (qb *Register) Validate() error {
	if qb.Account == "" {
		return errors.New("invalid account")
	}
	if qb.Pwd == "" {
		return errors.New("miss pwd")
	}
	if qb.Perm == "" {
		return errors.New("miss perm")
	}
	if qb.Key == "" {
		return errors.New("miss key")
	}
	return nil
}

type ChangePwd struct {
	New, Old string
}

func (qb *ChangePwd) Validate() error {
	if qb.New == "" {
		return errors.New("missing new pwd")
	}
	if qb.Old == "" {
		return errors.New("missing old pwd")
	}
	return nil
}
