package input

import (
	"errors"
	"remora-go/model/labor/dao"
	"strconv"
)

type CreateLaborIns struct {
	Name         string
	dao.LaborIns `json:",inline"`
}

func (i *CreateLaborIns) Validate() error {
	return nil
}

type QueryLaborIns struct {
	Year   int
	Salary int
	InsTyp dao.LaborInsType
	Days   uint8
}

func ParserQueryLaborIns(year, salary, days string, typ dao.LaborInsType) (*QueryLaborIns, error) {
	var err error
	ql := &QueryLaborIns{}
	ql.Year, err = strconv.Atoi(year)
	if err != nil {
		ql.Year = 0
	}
	ql.Salary, err = strconv.Atoi(salary)
	if err != nil {
		ql.Salary = 0
	}
	dayInt, err := strconv.Atoi(days)
	if err != nil {
		ql.Days = 30
	} else {
		ql.Days = uint8(dayInt)
	}
	ql.InsTyp = typ
	return ql, nil
}

func (q *QueryLaborIns) Validate() error {
	if q.Days > 30 {
		return errors.New("days must smaller than 30")
	}

	if q.Salary <= 0 {
		return errors.New("salary must bigger than 0")
	}
	if q.Year <= 0 {
		return errors.New("year must bigger than 0")
	}
	return nil
}
